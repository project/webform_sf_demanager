<?php

namespace Drupal\webform_sf_demanager\Plugin\WebformHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Send information to DEManager from a webform submission.
 *
 * @WebformHandler(
 *   id = "Send to DEManager",
 *   label = @Translation("Send to DEManager"),
 *   category = @Translation("External"),
 *   description = @Translation("Send information to DEManager from a webform submission."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_REQUIRED,
 * )
 */
class SendToDEManagerWebformHandler extends WebformHandlerBase {

  /**
   *
   * @var \Drupal\Core\Language\LanguageManager
   */
  protected $languageManager;

  /**
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   *
   */
  protected $httpClient;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->httpClient = $container->get('http_client');
    $instance->languageManager = $container->get('language_manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'baseConfig' => [
        'salesforce_url' => '',
        '_deExternalKey' => '',
        '_clientID' => '',
        'other_fields' => '',
      ],
      'redirection' => [
        'on_success' => '',
        'on_error' => '',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['baseConfig'] = [
      '#type'   => 'fieldset',
      '#title'  => t('Initial Webform - SalesForce DEManager settings'),
      '#weight' => -10,
      '#prefix' => '<div id="ajax-form-wrapper-cfgfirst">',
      '#suffix' => '</div>',
    ];

    $form['baseConfig']['salesforce_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Salesforce URL'),
      '#description' => $this->t('URL for your Marketing Cloud DEManager'),
      '#default_value' => $this->configuration['baseConfig']['salesforce_url'],
      '#required' => TRUE,
    ];
    $form['baseConfig']['_deExternalKey'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Data Extension name'),
      '#description' => $this->t('Name of your Data Extension in Marketing Cloud (_deExternalKey)'),
      '#default_value' => $this->configuration['baseConfig']['_deExternalKey'],
      '#required' => TRUE,
    ];
    $form['baseConfig']['_clientID'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client ID'),
      '#description' => $this->t('Your client ID'),
      '#default_value' => $this->configuration['baseConfig']['_clientID'],
      '#required' => TRUE,
    ];
    $form['baseConfig']['other_fields'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Mapping for other fields'),
      '#description' => $this->t('Field mapping, one per line, using this form: DEManager_field_name|webform_field_name'),
      '#default_value' => $this->configuration['baseConfig']['other_fields'],
      '#required' => TRUE,
    ];

    $form['redirection'] = [
      '#type'   => 'fieldset',
      '#title'  => t('Redirection management'),
      '#weight' => -10,
      '#prefix' => '<div id="ajax-form-wrapper-cfgredirection">',
      '#suffix' => '</div>',
    ];

    $form['redirection']['on_success'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('URL location "on_success"'),
      '#description'   => $this->t('Enter the URL to use when DEManager return successful response'),
      '#default_value' => $this->configuration['redirection']['on_success'],
      '#required'      => TRUE,
    ];

    $form['redirection']['on_error'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('URL location "on_error"'),
      '#description'   => $this->t('Enter the URL to use when DEManager return unsuccessful response'),
      '#default_value' => $this->configuration['redirection']['on_error'],
      '#required'      => TRUE,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $form_values = $form_state->getValues();
    foreach ($form_values as $key => $form_value) {
      $this->configuration[$key] = $form_value;
    }
  }

  /**
   * Implements form validation.
   *
   * Drupal Ajax have a pre-validation process on the same page, but this function validate field on specific criteria before submit.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $validator = [
      "salesforce_url"  => [
        "error_message" => $this->t("Please enter a valid 'URL for your Marketing Cloud DEManager'"),
        "check" => ["isUrl"],
      ],
      "_deExternalKey"  => [
        "error_message" => $this->t("Please enter a valid 'Name of your Data Extension' in Marketing Cloud"),
        "check" => ["isAlpha"],
      ],
      "_clientID"  => [
        "error_message" => $this->t("Please enter a valid 'client ID'"),
        "check" => ["isAlpha"],
      ],
      "other_fields"  => [
        "error_message" => $this->t("Please enter your field mapping, one per line, by respecting this form : DEManager_field_name|webform_field_name"),
        "check" => ["isFieldlist"],
      ],
      "on_success"  => [
        "error_message" => $this->t("Enter a valid URL to use when DEManager return successful response"),
        "check" => ["isUrl"],
      ],
      "on_error"  => [
        "error_message" => $this->t("Enter a valid URL to use when DEManager return unsuccessful response"),
        "check" => ["isUrl"],
      ],
    ];
    $message = parent::validateConfigurationForm($form, $form_state);

    $values = $form_state->getValues();
    foreach ($values as $fieldset => $index) {
      foreach ($index as $key => $value) {
        if (isset($validator["$key"])) {
          foreach ($validator["$key"]["check"] as $function2call) {
            if (!$this->$function2call($value, $key)) {
              $form_state->setErrorByName($key, $validator["$key"]["error_message"]);
            }
          }
        }
      }
    }

    return $message;
  }

  /**
   * Implements form validation.
   * 
   * Drupal validation process; validate field on specific criteria before submit.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   */
  public function validateForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) {
    parent::validateForm($form, $form_state, $webform_submission);
    $values = $form_state->getValues();

    if (!$form_state->hasAnyErrors() ) {
      // No error, so now try "poking" DEManager to send things there.
      $successUrl = $this->configuration['redirection']['on_success'];
      $errorUrl = $this->configuration['redirection']['on_error'];

      $cfgFields = $this->splitTextArea($this->configuration['baseConfig']['other_fields']);

      $fields = [];

      // Here, we try builting the query parameters for http_client request.
      // we need to join the cfgField to the value' array.
      foreach ($cfgFields as $cfgField) {
        if (preg_match('/\|/', $cfgField)) {
          $parts = explode('|', $cfgField);
          if (intval($this->removeCarriageReturn($parts[1]))) {
            $fields[$parts[0]] = $this->removeCarriageReturn($parts[1]);
          }
          else {
            if (isset($values[$this->removeCarriageReturn($parts[1])])) {
              $fields[$parts[0]] = $values[$this->removeCarriageReturn($parts[1])];
            }
            else {
              $form_state->setErrorByName($this->removeCarriageReturn($parts[1]), $this->t('Seem that this parameter will cause error. Please contact the administrator.'));
            }
          }
        }
      }

      $otherFields = [
        "_successURL" => $successUrl,
        "_errorURL" => $errorUrl,
        // @todo should _action and _returnXML must be in settings ?
        "_action" => "add",
        "_returnXML" => 0,
        '_deExternalKey' => $this->configuration['baseConfig']['_deExternalKey'],
        '_clientID' => $this->configuration['baseConfig']['_clientID'],
      ];

      $fields = array_merge($fields, $otherFields);

      try {
        $request = $this->httpClient->request(
          'POST',
          $this->configuration['baseConfig']['salesforce_url'],
          ['form_params' => $fields, 'allow_redirects' => FALSE]
        );

        if ($request->getStatusCode() && $request->getStatusCode() != 302) {
          $form_state->setErrorByName("RemotePost", $this->t('An Error Occured while adding the email. Please, contact the Administrator.'));
        }
        else {
          if (!$request->hasHeader('location') && in_array($successUrl, $request->getHeader('location'))
              || !$request->hasHeader('Location') && in_array($successUrl, $request->getHeader('Location'))) {
            $form_state->setErrorByName("RemotePost", $this->t('An Error Occured while adding the email. Please, contact the Administrator.'));
          }
        }
      }
      catch (RequestException $e) {
        $form_state->setErrorByName("RemotePost", $this->t('An Error Occured. Please, contact the Administrator.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) {
    // @todo NEED REWORK FOR FORM RESULT MANAGEMENET
    //$values = $webform_submission->getData();
    //$webform_submission->setElementData($this->configuration, $values);
  }

  /**
   * Validate if value is numeric.
   *
   * @param string $value
   * @param $key
   *
   * @return bool
   */
  protected function isNum($value, $key = NULL) {
    return is_numeric($value);
  }

  /**
   * Test if value is alphanumeric or with permitted characters.
   *
   * @param string $value
   * @param $key
   *
   * @return bool
   */
  protected function isAlpha($value, $key = NULL) {
    if (preg_match('/[^A-Z\'a-z\_\.\@\ \-0-9àèìòùÀÈÌÒÙáéíóúýÁÉÍÓÚÝâêîôûÂÊÎÔÛãñõÃÑÕäëïöüÿÄËÏÖÜŸçÇßØøÅåÆæœ]/i', $value)) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Validate if value is alphanumeric without accent.
   *
   * @param string $value
   * @param $key
   *
   * @return bool
   */
  protected function isAlphaNoAccent($value, $key = NULL) {
    if (preg_match('/[^A-Za-z\_\.\@\ \-0-9]/i', $value)) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Validate if url have a valid scheme and HTTPS.
   *
   * @param string $value
   * @param $key
   *
   * @return bool
   */
  protected function isUrl($value, $key = NULL) {
    if (filter_var($value, FILTER_VALIDATE_URL)) {
      if (preg_match('/https:\/\//', $value)) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Validate if the value can be a valid link.
   *
   * @param string $value
   * @param $key
   *
   * @return bool
   */
  protected function isFieldlist($value, $key = NULL) {

    $lines = $this->splitTextArea($value);

    foreach ($lines as $line) {
      // Search for separator.
      if (!preg_match('/\|/', $line)) {
        return FALSE;
      }

      $fields = explode('|', $line);

      if (count($fields) > 2) {
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   *
   */
  protected function splitTextArea($value) {
    $split = "\n";

    if (strpos("\r\n", $value)) {
      $split = "\r\n";
    }
    return explode($split, $value);
  }

  /**
   *
   */
  protected function removeCarriageReturn($text) {
    return preg_replace("/\r|\n/", "", $text);
  }

}
